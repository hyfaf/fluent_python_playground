# !/bin/python

def printStar(func):
    print("********start********")
    return func

def add():
    return 1 + 1

def sub():
    return 2 - 1

print(printStar(add()))
print(add())

print(printStar(sub()))
print(sub())

