#!/usr/bin/env python
# coding=utf-8
def factorial(n):
    '''return n'''
    return 1 if n < 2 else n* factorial(n-1)

print(factorial(10))
print(factorial.__doc__)
