# !/bin/python

def printStar(func):
    print("********start********")
    def f():
        print("**************")
        return func()
    return f

@printStar
def add():
    return 1 + 1

@printStar
def sub():
    return 2 - 1

i = add()
print(i)

print(sub())



